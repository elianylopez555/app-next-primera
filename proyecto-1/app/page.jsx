
import React from "react";
import Users from "@/components/Users";

//datos que se estan ejecutando desde el servidor
async function fetchUsers() {
 const res= await fetch("https://reqres.in/api/users")
 const data= await res.json()
  return data.data
}//se crea la pagina  empieza a solicitar los datos y se ejecuta la  funcion fetchUser
 async  function  IndexPage(){ 
 const users= await fetchUsers() //cuando ejecutes dame estos datos de usuario
  console.log(users)
 return(
    <div>
     
     <Users users={users}/>
    </div>
  )
}

export default IndexPage