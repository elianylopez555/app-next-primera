import Users from "@/components/Users"
//componentes renderizados del lado del servidor
async function getUser(id) {
    const res= await fetch(`https://reqres.in/api/users/${id}`)
    const data= await res.json()
    return data.data
   
}

async function UserPage({params}) {  
   const users =  await getUser(params.id)
    
 
   return(
    <div className="row">
     <div className="col-md-6 offset-md-3">
        <div className="card">
            <div className="card-header text-center">
              <img src={users.avatar}alt={users.email} style={{borderRadius:"50%"}} />
            </div>
            <div className="card-body text-center">
            
            <div>
                <h3>{users.id} {users.fist_name}</h3>
            <p>{users.email}</p>
            </div>
            

        </div>

        </div>
        </div>  

    </div>
   ) 
}
export default UserPage